package main

import (
	"errors"
	"reflect"
	"strconv"
	"sync/atomic"
	"testing"
	"time"
)

var testItems = []Item{
	{"+5 Dexterity Vest", 9, 19, currentTime},
	{"Aged Brie", 1, 1, currentTime},
	{"Elixir of the Mongoose", 4, 6, currentTime},
	{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
	{"Backstage passes to a TAFKAL80ETC concert", 14, 21, currentTime},
	{"Conjured Mana Cake", 2, 4, currentTime},
}

var tmpItems1 []Item

func TestNighlyUpdate(t *testing.T) {
	tmpItems1 = make([]Item, len(testItems))
	nightlyUpdate(false, &testIterator{}, mockItemUpdater1)
	if !reflect.DeepEqual(testItems, tmpItems1) {
		t.Error()
	}
}

var tmpItems2 []Item

func TestNighlyUpdateError(t *testing.T) {
	disableLog()

	nightlyUpdate(false, &testIterator{}, mockItemUpdater2)
	if callscnt2 != int64(len(testItems))*3 {
		t.Error(callscnt2)
	}
}

func TestSelectAll(t *testing.T) {
	itms := selectAll(&testIterator2{})
	if !reflect.DeepEqual(itms, items) {
		t.Error()
	}
}

var callscnt int64

func mockItemUpdater1(item iItem) error {
	atomic.AddInt64(&callscnt, 1)
	id, _ := strconv.Atoi(item.ID)
	tmpItems1[id] = item.Item
	return nil
}

var callscnt2 int64

func mockItemUpdater2(item iItem) error {
	atomic.AddInt64(&callscnt2, 1)
	return errors.New("random error")
}

type testIterator struct {
	called int
}

func (ti *testIterator) Do(force bool, date time.Time) {
	ti.called = -1
}
func (ti *testIterator) Next() bool {
	if ti.called < len(testItems)-1 {
		ti.called++
		return true
	}
	return false
}
func (ti *testIterator) Value() (*Item, string) {
	return &items[ti.called], strconv.Itoa(ti.called)
}

type testIterator2 struct {
	called int
}

func (ti *testIterator2) Do() {
	ti.called = -1
}
func (ti *testIterator2) Next() bool {
	if ti.called < len(testItems)-1 {
		ti.called++
		return true
	}
	return false
}
func (ti *testIterator2) Value() *Item {
	return &items[ti.called]
}
