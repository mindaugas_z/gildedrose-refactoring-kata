# GO Starter


Run: 
```
docker run --rm -d -p 9200:9200 -p 5601:5601 nshou/elasticsearch-kibana

go get github.com/olivere/elastic
go get github.com/pkg/errors
go get github.com/Sirupsen/logrus

go run $(ls *.go|grep -v _test) --es http://127.0.0.1:9200 --cron --bind :8082 --delete-index --create-index --debug [--fake-date]

curl http://localhost:8082/items.json // list items
curl http://localhost:8082/_update // can be called from os cron if no internal cron running
curl http://localhost:8082/_force_update_rng 
```

- Run tests :

```shell
go test -v --cover
```

- Run tests and coverage :

```shell
go test -coverprofile=coverage.out

go tool cover -html=coverage.out
```