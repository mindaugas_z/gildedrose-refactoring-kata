package main

import (
	"encoding/json"
	"flag"

	"net/http"
	"sync"
	"time"
)

var currentTime time.Time = getRoundedDate()

type Item struct {
	Name      string    `json:"name"`
	SellIn    int       `json:"sellin"`
	Quality   int       `json:"quality"`
	UpdatedAt time.Time `json:"updated_at"`
}

func (i *Item) Encode() ([]byte, error) {
	return json.Marshal(*i)
}
func (i *Item) Decode(b []byte) {
	json.Unmarshal(b, i)
}

var items = []Item{
	Item{"+5 Dexterity Vest", 10, 20, currentTime},
	Item{"Aged Brie", 2, 0, currentTime},
	Item{"Elixir of the Mongoose", 5, 7, currentTime},
	Item{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
	Item{"Backstage passes to a TAFKAL80ETC concert", 15, 20, currentTime},
	Item{"Conjured Mana Cake", 3, 6, currentTime},
}

var mu sync.Mutex

var esBackend = flag.String("es", "http://127.0.0.1:9200", "-es http://127.0.0.1:9200")
var internalCron = flag.Bool("cron", false, "-cron")
var bind = flag.String("bind", ":8082", "-bind :8082")
var deleteIndex = flag.Bool("delete-index", false, "-delete-index")
var createIndex = flag.Bool("create-index", false, "-create-index")
var debugLevel = flag.Bool("debug", false, "-debug")
var fakeDate = flag.Bool("fake-date", false, "-fake-date")

func main() {
	flag.Parse()

	SetLogLevel()

	if err := ESInit(); err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/items.json", func(w http.ResponseWriter, r *http.Request) {
		b, _ := json.MarshalIndent(SelectAll(), "", "  ")
		w.Header().Set("Content-Type", "application/json")
		w.Write(b)
	})

	// can be called from os crontab
	http.HandleFunc("/_update", func(w http.ResponseWriter, r *http.Request) {
		go NightlyUpdate(false)
		w.Write([]byte("Queued"))
	})

	http.HandleFunc("/_force_update_rng", func(w http.ResponseWriter, r *http.Request) {
		go NightlyUpdate(true)
		w.Write([]byte("Queued"))
	})

	if *internalCron {
		cronStart()
	}

	log.Fatal(http.ListenAndServe(*bind, nil))
}

func GildedRose2() {
	for i := 0; i < len(items); i++ {
		process(&items[i])
	}
}

func process(i *Item) {
	if fn, ok := fns[i.Name]; ok {
		fn(i)
		return
	}
	pdef(i)
}

var fns = map[string]func(*Item){
	"Aged Brie":                                 p2,
	"Sulfuras, Hand of Ragnaros":                noop,
	"Backstage passes to a TAFKAL80ETC concert": p5,
	"Conjured Mana Cake":                        p6,
}

// Sulfuras, Hand of Ragnaros
func noop(item *Item) {}

// simple product, default
func pdef(item *Item) {
	DecreaseQuality(item)
	DecreaseSellin(item)
	if item.SellIn < 0 {
		DecreaseQuality(item)
	}
}

// Conjured Mana Cake
func p6(item *Item) {
	DecreaseQuality(item)
	pdef(item)
}

// Aged Brie
func p2(item *Item) {
	IncreaseQuality(item)
	DecreaseSellin(item)
	if item.SellIn < 0 {
		IncreaseQuality(item)
	}
}

// Backstage passes to a TAFKAL80ETC concert
func p5(item *Item) {
	IncreaseQuality(item)
	if item.SellIn < 11 {
		IncreaseQuality(item)
	}
	if item.SellIn < 6 {
		IncreaseQuality(item)
	}
	DecreaseSellin(item)
	if item.SellIn < 0 {
		item.Quality = 0
	}
}

func DecreaseSellin(item *Item) {
	item.SellIn--
}

func IncreaseQuality(item *Item) {
	if item.Quality < 50 {
		item.Quality++
	}
}

func DecreaseQuality(item *Item) {
	if item.Quality > 0 {
		item.Quality--
	}
}
