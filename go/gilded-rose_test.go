package main

import (
	"reflect"
	"testing"
)

var Items_asis = [][]Item{
	{
		Item{"+5 Dexterity Vest", 9, 19, currentTime},
		Item{"Aged Brie", 1, 1, currentTime},
		Item{"Elixir of the Mongoose", 4, 6, currentTime},
		Item{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
		Item{"Backstage passes to a TAFKAL80ETC concert", 14, 21, currentTime},
		Item{"Conjured Mana Cake", 2, 4, currentTime},
	},
	{
		Item{"+5 Dexterity Vest", 8, 18, currentTime},
		Item{"Aged Brie", 0, 2, currentTime},
		Item{"Elixir of the Mongoose", 3, 5, currentTime},
		Item{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
		Item{"Backstage passes to a TAFKAL80ETC concert", 13, 22, currentTime},
		Item{"Conjured Mana Cake", 1, 2, currentTime},
	},
	{
		Item{"+5 Dexterity Vest", 7, 17, currentTime},
		Item{"Aged Brie", -1, 4, currentTime},
		Item{"Elixir of the Mongoose", 2, 4, currentTime},
		Item{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
		Item{"Backstage passes to a TAFKAL80ETC concert", 12, 23, currentTime},
		Item{"Conjured Mana Cake", 0, 0, currentTime},
	},
	{
		Item{"+5 Dexterity Vest", 6, 16, currentTime},
		Item{"Aged Brie", -2, 6, currentTime},
		Item{"Elixir of the Mongoose", 1, 3, currentTime},
		Item{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
		Item{"Backstage passes to a TAFKAL80ETC concert", 11, 24, currentTime},
		Item{"Conjured Mana Cake", -1, 0, currentTime},
	},
	{
		Item{"+5 Dexterity Vest", 5, 15, currentTime},
		Item{"Aged Brie", -3, 8, currentTime},
		Item{"Elixir of the Mongoose", 0, 2, currentTime},
		Item{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
		Item{"Backstage passes to a TAFKAL80ETC concert", 10, 25, currentTime},
		Item{"Conjured Mana Cake", -2, 0, currentTime},
	},
	{
		Item{"+5 Dexterity Vest", 4, 14, currentTime},
		Item{"Aged Brie", -4, 10, currentTime},
		Item{"Elixir of the Mongoose", -1, 0, currentTime},
		Item{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
		Item{"Backstage passes to a TAFKAL80ETC concert", 9, 27, currentTime},
		Item{"Conjured Mana Cake", -3, 0, currentTime},
	},
	{
		Item{"+5 Dexterity Vest", 3, 13, currentTime},
		Item{"Aged Brie", -5, 12, currentTime},
		Item{"Elixir of the Mongoose", -2, 0, currentTime},
		Item{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
		Item{"Backstage passes to a TAFKAL80ETC concert", 8, 29, currentTime},
		Item{"Conjured Mana Cake", -4, 0, currentTime},
	},
	{
		Item{"+5 Dexterity Vest", 2, 12, currentTime},
		Item{"Aged Brie", -6, 14, currentTime},
		Item{"Elixir of the Mongoose", -3, 0, currentTime},
		Item{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
		Item{"Backstage passes to a TAFKAL80ETC concert", 7, 31, currentTime},
		Item{"Conjured Mana Cake", -5, 0, currentTime},
	},
	{
		Item{"+5 Dexterity Vest", 1, 11, currentTime},
		Item{"Aged Brie", -7, 16, currentTime},
		Item{"Elixir of the Mongoose", -4, 0, currentTime},
		Item{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
		Item{"Backstage passes to a TAFKAL80ETC concert", 6, 33, currentTime},
		Item{"Conjured Mana Cake", -6, 0, currentTime},
	},
	{
		Item{"+5 Dexterity Vest", 0, 10, currentTime},
		Item{"Aged Brie", -8, 18, currentTime},
		Item{"Elixir of the Mongoose", -5, 0, currentTime},
		Item{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
		Item{"Backstage passes to a TAFKAL80ETC concert", 5, 35, currentTime},
		Item{"Conjured Mana Cake", -7, 0, currentTime},
	},
	{
		Item{"+5 Dexterity Vest", -1, 8, currentTime},
		Item{"Aged Brie", -9, 20, currentTime},
		Item{"Elixir of the Mongoose", -6, 0, currentTime},
		Item{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
		Item{"Backstage passes to a TAFKAL80ETC concert", 4, 38, currentTime},
		Item{"Conjured Mana Cake", -8, 0, currentTime},
	},
	{
		Item{"+5 Dexterity Vest", -2, 6, currentTime},
		Item{"Aged Brie", -10, 22, currentTime},
		Item{"Elixir of the Mongoose", -7, 0, currentTime},
		Item{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
		Item{"Backstage passes to a TAFKAL80ETC concert", 3, 41, currentTime},
		Item{"Conjured Mana Cake", -9, 0, currentTime},
	},
	{
		Item{"+5 Dexterity Vest", -3, 4, currentTime},
		Item{"Aged Brie", -11, 24, currentTime},
		Item{"Elixir of the Mongoose", -8, 0, currentTime},
		Item{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
		Item{"Backstage passes to a TAFKAL80ETC concert", 2, 44, currentTime},
		Item{"Conjured Mana Cake", -10, 0, currentTime},
	},
	{
		Item{"+5 Dexterity Vest", -4, 2, currentTime},
		Item{"Aged Brie", -12, 26, currentTime},
		Item{"Elixir of the Mongoose", -9, 0, currentTime},
		Item{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
		Item{"Backstage passes to a TAFKAL80ETC concert", 1, 47, currentTime},
		Item{"Conjured Mana Cake", -11, 0, currentTime},
	},
	{
		Item{"+5 Dexterity Vest", -5, 0, currentTime},
		Item{"Aged Brie", -13, 28, currentTime},
		Item{"Elixir of the Mongoose", -10, 0, currentTime},
		Item{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
		Item{"Backstage passes to a TAFKAL80ETC concert", 0, 50, currentTime},
		Item{"Conjured Mana Cake", -12, 0, currentTime},
	},
	{
		Item{"+5 Dexterity Vest", -6, 0, currentTime},
		Item{"Aged Brie", -14, 30, currentTime},
		Item{"Elixir of the Mongoose", -11, 0, currentTime},
		Item{"Sulfuras, Hand of Ragnaros", 0, 80, currentTime},
		Item{"Backstage passes to a TAFKAL80ETC concert", -1, 0, currentTime},
		Item{"Conjured Mana Cake", -13, 0, currentTime},
	},
}

func Test_GildedRose(t *testing.T) {
	for i, asis := range Items_asis {
		GildedRose2()
		if !reflect.DeepEqual(items, asis) {
			t.Errorf("iteration #%d", i+1)
			t.Errorf("Got: %#v", items)
			t.Errorf("Expected %#v", asis)
		}
	}
}
