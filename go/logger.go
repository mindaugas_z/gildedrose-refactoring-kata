package main

import (
	slog "github.com/Sirupsen/logrus"
	"io/ioutil"
)

var log *slog.Logger = slog.New()

func SetLogLevel() {
	log.SetLevel(slog.InfoLevel)

	if *debugLevel {
		log.SetLevel(slog.DebugLevel)
	}
}

func disableLog() {
	log.SetOutput(ioutil.Discard)
}
