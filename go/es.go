package main

import (
	"context"
	"encoding/json"
	"sync"
	"sync/atomic"
	"time"

	"io"
	"reflect"
	"strconv"

	"github.com/olivere/elastic"
	"github.com/pkg/errors"
)

const mapping = `
{
	"settings":{
		"number_of_shards": 1,
		"number_of_replicas": 0
	},
	"mappings":{
		"items":{
			"properties":{
				"name":{
					"type":"keyword"
				},
				"sellin":{
					"type":"integer"
				},
				"quality":{
					"type":"integer"
				},
				"updated_at":{
					"type":"date"
				}
			}
		}
	}
}`

var (
	esctx    context.Context
	esclient *elastic.Client
)

func ESInit() error {
	esctx = context.Background()
	var err error

	esclient, err = elastic.NewSimpleClient(elastic.SetURL(*esBackend))
	if err != nil {
		// Handle error
		return errors.Wrap(err, "ESInit NewSimpleClient")
	}

	info, code, err := esclient.Ping(*esBackend).Do(esctx)
	if err != nil {
		// Handle error
		return errors.Wrap(err, "ESInit ping")
	}
	log.Printf("Elasticsearch returned with code %d and version %s\n", code, info.Version.Number)

	esversion, err := esclient.ElasticsearchVersion(*esBackend)
	if err != nil {
		// Handle error
		return errors.Wrap(err, "ESInit ESVersion")
	}
	log.Printf("Elasticsearch version %s\n", esversion)

	exists, err := esclient.IndexExists("items").Do(esctx)
	if err != nil {
		// Handle error
		return errors.Wrap(err, "ESInit IndexExists")
	}
	if exists && *deleteIndex {
		log.Print("Deleting index")
		if _, err := esclient.DeleteIndex("items").Do(esctx); err != nil {
			return errors.Wrap(err, "ESInit DeleteIndex")
		}
		exists = false
	}
	var errcnt int
	if !exists && *createIndex {
		// Create a new index.
		createIndex, err := esclient.CreateIndex("items").BodyString(mapping).Do(esctx)
		if err != nil {
			// Handle error
			return errors.Wrap(err, "ESInit CreateIndex")
		}
		if !createIndex.Acknowledged {
			// Not acknowledged
		}

		for id, item := range items {
			b, err := item.Encode()
			if err != nil {
				errcnt++
				log.Errorf("CreateItems: DecodeItem error: %v", err)
				continue
			}

			if _, err := esclient.Index().Index("items").Type("items").Id(strconv.Itoa(id)).BodyJson(string(b)).Do(esctx); err != nil {
				errcnt++
				log.Errorf("CreateItems: SaveItem error: %v", err)
			}

		}
	}
	if errcnt != 0 {
		return errors.New("Found some errors while creating items")
	}
	return nil
}

// works with small amount of items.

func SelectAll() []Item {
	return selectAll(&Iter2{})
}

func selectAll(itr IteratorInterface2) []Item {
	items := []Item{}

	itr.Do()
	for itr.Next() {
		if item := itr.Value(); item != nil {
			items = append(items, *item)
		}
	}
	return items
}

type iItem struct {
	ID   string
	Item Item
}

var updateRunning uint32

func NightlyUpdate(force bool) {
	nightlyUpdate(force, &Iter1{}, updateItem)
}
func nightlyUpdate(force bool, itr IteratorInterface1, itemUpdater func(iItem) error) {
	if !atomic.CompareAndSwapUint32(&updateRunning, 0, 1) {
		log.Debug("NighlyUpdate already running")
		return
	}
	defer func() { atomic.StoreUint32(&updateRunning, 0) }()

	var errcnt int64

	itemsChannel := make(chan iItem)

	currentDate := getRoundedDate()
	if *fakeDate {
		currentDate = currentDate.Add(24 * time.Hour)
	}
	var wg sync.WaitGroup
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func(i int, t time.Time) {
			defer wg.Done()
			log.Debugf("Sarting worker #%d", i)
			for item := range itemsChannel {
				log.Debugf("NightlyUpdate worker #%d pre-process %#v", i, item.Item)
				process(&item.Item)
				item.Item.UpdatedAt = t
				log.Debugf("NightlyUpdate worker #%d post-process %#v", i, item.Item)
				retry := 1
				for {
					if err := itemUpdater(item); err != nil {
						log.Errorf("NighlyUpdate worker #%d save item error: %v, try: %v", i, retry, err)
						atomic.AddInt64(&errcnt, 1)
						retry++
						if retry <= 3 {
							time.Sleep(1 * time.Second)
							continue
						}
					}
					break
				}
			}
			log.Debugf("Exiting worker #%d", i)
		}(i, currentDate)
	}

	itr.Do(force, currentDate)
	for itr.Next() {
		if item, id := itr.Value(); item != nil {
			itemsChannel <- iItem{id, *item}
		}
	}

	if !force {
		if errcnt > 0 {
			go func() {
				log.Print("Found errors while updating items. Auto rerun after 5 minutes")
				time.Sleep(5 * time.Minute)
				go NightlyUpdate(false) // tricky
			}()
		}
	}
	close(itemsChannel)
	wg.Wait()
}

type IteratorInterface1 interface {
	Do(bool, time.Time)
	Next() bool
	Value() (*Item, string)
}

type IteratorInterface2 interface {
	Do()
	Next() bool
	Value() *Item
}

type Iter1 struct {
	ss *elastic.ScrollService
	sr *elastic.SearchResult
}

func (i *Iter1) Do(force bool, currentDate time.Time) {
	query := elastic.NewBoolQuery()
	if !force {
		query.MustNot(elastic.NewTermQuery("updated_at", currentDate))
	}

	i.ss = esclient.Scroll("items").Type("items").Size(1).Query(query)
}
func (i *Iter1) Next() bool {
	var err error
	i.sr, err = i.ss.Do(esctx)
	if err == io.EOF {
		return false
	}
	return err == nil
}
func (i *Iter1) Value() (*Item, string) {
	var itm Item
	for _, item := range i.sr.Hits.Hits {
		err := json.Unmarshal(*item.Source, &itm)
		if err != nil {
			log.Debugf("NightlyUpdate unmarshal item error: %v", err)
			continue
		}
		return &itm, item.Id
	}
	return nil, ""
}

type Iter2 struct {
	ss *elastic.ScrollService
	sr *elastic.SearchResult
}

func (i *Iter2) Do() {
	i.ss = esclient.Scroll("items").Type("items").Size(1)
}
func (i *Iter2) Next() bool {
	var err error
	i.sr, err = i.ss.Do(esctx)
	if err == io.EOF {
		return false
	}
	return err == nil
}
func (i *Iter2) Value() *Item {
	var itm Item
	for _, item := range i.sr.Each(reflect.TypeOf(itm)) {
		if i, ok := item.(Item); ok {
			return &i
		}
	}
	return nil
}

func updateItem(item iItem) error {
	_, err := esclient.Update().Index("items").Type("items").Id(item.ID).Doc(item.Item).Do(esctx)
	return err
}

func getRoundedDate() time.Time {
	ts := time.Now()
	return time.Date(ts.Year(), ts.Month(), ts.Day(), 0, 0, 0, 0, ts.UTC().Location())
}
