package main

import (
	"io/ioutil"
	"strconv"
	"time"
)

func cronStart() {
	var initialRun bool
	nextRun := int64(0)
	b, err := ioutil.ReadFile("crontab")
	if err == nil {
		nr, _ := strconv.Atoi(string(b))
		nextRun = int64(nr)
	}

	go func() {
		for {
			time.Sleep(1 * time.Second)
			ts := time.Now()
			rounded := time.Date(ts.Year(), ts.Month(), ts.Day(), 1, 0, 0, 0, ts.UTC().Location())
			if nextRun < rounded.UnixNano() || !initialRun {
				initialRun = true
				nextRun = rounded.UnixNano() + 24*time.Hour.Nanoseconds()
				ioutil.WriteFile("crontab", []byte(strconv.Itoa(int(nextRun))), 0755)
				log.Println("Nightly update")
				NightlyUpdate(false)
			}
		}
	}()
}
